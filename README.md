# Twig

If you'd like to run this through the browser, I stuck it on trinket.io [here!](https://trinket.io/python3/43fbe164d4)
If you'd like to run it locally feel free to download it and run `python arraySplit.py`!
The unit tests can be run locally via `python -m unittest tests.py`!

As for the implementation, I went for numpy's array split method, as that seemed like the most fullproof solution especially for code that will actually be used in production. To prevent any compatibility issues down the line, the version of numpy used should be captured (in this case with the requirements file), stopping any breaking changes being introduced from later versions of numpy.

For the unit tests, I have little experience with python unit testing but am pleasantly surprised with the unittest suite. I just covered some basic expected/edge cases to show proof of testing. 

I spent roughly an hour on this and for things I would improve upon given more time, I'm thinking the following:
- The folder hierarchy is flat atm, this should be separated into src and test code.
- I'm not familiar with proper error handling and logging in python, so learning about that would help the user validation, guarding around edge cases.
- I'm also not sure which linting convention you follow, I mostly just followed the PeP 8 style conventions, but whatever the team uses/is familiar with is obviously best.