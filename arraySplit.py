import numpy


def groupArrayElements(input_array, divider):
    """Return the contents of an array divided into N equally sized lists.

    If the original array cannot be divided equally by N, the final part's length will equal the remainder.

    :param input_array: the array to divide up
    :param divider: the number of sub-arrays to divide into
    :return: a list containing n lists.
    """

    if divider == 0:
        return []
    return convertSubArraysToLists(numpy.array_split(input_array, divider))


def convertSubArraysToLists(input_list):
    """Convert a list with multiple sub-arrays to a list with multiple sub-lists.

    :param input_list: the list with sub-arrays to convert
    :return: the list of lists
    """
    output_list = []
    if len(input_list) == 0:
        return input_list
    for subArray in input_list:
        output_list.append(subArray.tolist())
    return output_list


def getUserInput():
    """Gets user input and validates it.

    :return: The user validated input
    """
    while True:
        try:
            comma_separated_ints = input('Enter your array as numbers separated by commas: ')
            array = [int(num) for num in comma_separated_ints.split(',')]
            break
        except ValueError:
            print("That's not a comma separated int list!")
    while True:
        try:
            divisor = int(input('Enter your divisor: '))
            break
        except ValueError:
            print("That's not an int!")
    return array, divisor


def main():
    """Prints the output of groupArrayElements after getting and validating the user inputs."""
    array, divisor = getUserInput()
    print(groupArrayElements(array, divisor))


if __name__ == '__main__':
    main()
