import unittest
import numpy
import arraySplit


class TestStringMethods(unittest.TestCase):

    def test_base_case(self):
        result = arraySplit.groupArrayElements([1, 2, 3, 4, 5], 3)
        self.assertEqual(result, [[1, 2], [3, 4], [5]])

    def test_even_divide(self):
        result = arraySplit.groupArrayElements([1, 2, 3, 4, 5, 6], 3)
        self.assertEqual(result, [[1, 2], [3, 4], [5, 6]])

    def test_divider_larger_than_array(self):
        result = arraySplit.groupArrayElements([1, 2, 3, 4, 5, 6], 10)
        self.assertEqual(result, [[1], [2], [3], [4], [5], [6], [], [], [], []])

    def test_zero_divider(self):
        result = arraySplit.groupArrayElements([1, 2, 3, 4, 5], 0)
        self.assertEqual(result, [])

    def test_huge_array(self):
        result = arraySplit.groupArrayElements(numpy.ones(100000), 3)
        self.assertEqual(len(result), 3)